# Dash.jl tricks

Collection of example [Dash.jl](https://github.com/plotly/Dash.jl) apps
showcasing tricks that do not (and probably should not :smirk:) make their
way into the official [docs](https://dash.plotly.com/julia).

The full list of examples [here](./examples/README.md).

## See also

- Quarto project for my JuliaCon 2023 presentation: [_Tips for writing and maintaining Dash.jl applications_][juliacon2023]
- Tutorial and example Julia package on [Dash.jl multi-page apps][multipage]
- [`DashTextareaAutocomplete`][DashTextareaAutocomplete], a component wrapper for [`@webscopeio/react-textarea-autocomplete`][webscopeio]

## Code style

[![SciML Code Style](https://img.shields.io/static/v1?label=code%20style&message=SciML&color=9558b2&labelColor=389826)](https://github.com/SciML/SciMLStyle)
[![JavaScript Style Guide](https://cdn.rawgit.com/standard/standard/master/badge.svg)](https://github.com/standard/standard)

## License

[MIT](./LICENSE) - etpinard

[juliacon2023]: https://gitlab.com/etpinard/dash.jl-tricks/-/tree/main/slides/juliacon2023
[multipage]: https://gitlab.com/etpinard/dash.jl-multi-page-app-example
[DashTextareaAutocomplete]: https://github.com/etpinard/dash-textarea-autocomplete
[webscopeio]: https://github.com/webscopeio/react-textarea-autocomplete
