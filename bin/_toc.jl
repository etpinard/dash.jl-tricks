const PATH_EXAMPLES = joinpath(@__DIR__, "..", "examples")

function list_examples()
    return sort(filter(isdir, readdir(PATH_EXAMPLES; join=true)); by=mtime)
end

function read_ex_readme(path)
    try
        return read(joinpath(path, "README.md"), String)
    catch err
        @error "Something went went trying to read the $path README.md" err
    end
end

function find_title(path)
    ex_readme = read_ex_readme(path)
    line1 = split(ex_readme, "\n") |> first |> string
    pieces = split(line1, "# ") .|> string
    @assert isempty(pieces[1]) "first line should have a H1"
    return pieces[2]
end

is_heading(l) = !isempty(l) && startswith(l, "#")

function find_description(path)
    ex_readme = read_ex_readme(path)
    lines = split(ex_readme, "\n") .|> string
    return if length(lines) == 1
        ""
    else
        lines = lines[2:end]
        idx = findfirst(is_heading, lines)
        join(if isnothing(idx)
            lines
        else
            lines[2:idx-1]
        end, "\n")
    end
end

function make_readme(examples)
    header = "# Dash.jl examples table of content"
    items = mapreduce(vcat, examples) do path
        href = "./" * relpath(path, PATH_EXAMPLES)
        title = find_title(path)
        description = find_description(path)
        ["## [$title]($href)", description]
    end
    return join(vcat(header, items), "\n\n")
end

function main(; outpath = joinpath(PATH_EXAMPLES, "README.md"))
    examples = list_examples()
    @assert length(examples) > 0 "examples folder is empty"
    readme = make_readme(examples)
    write(outpath, readme)
end
