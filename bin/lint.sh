#! /bin/bash

HERE=$(dirname "$(readlink -f "$0")")

lint_js () {
    npx -y standard@17.1.0 --fix examples
}

lint_julia () {
    julia --project="$HERE"/env -e 'using JuliaFormatter; format(["examples", "slides", "bin"]);'
}

lint_sh () {
    # N.B. just a check here, does not try to modify to files
    shellcheck bin/*.sh
}

cd "$HERE"/.. || exit 1
case $1 in
    js)
        lint_js
        ;;
    julia | jl)
        lint_julia
        ;;
    bash | shell | sh)
        lint_sh
        ;;
    *)
        lint_js
        lint_julia
        lint_sh
        ;;
esac
