#! /bin/bash

HERE=$(dirname "$(readlink -f "$0")")

pushd "$HERE" > /dev/null || exit 1
julia --project=env -e 'include("_toc.jl"); main()'
popd > /dev/null || exit 1
