# Dash.jl examples table of content

## [Keyboard callbacks in Dash.jl](./keyboard-callback)

Dash.jl nor the python version of dash support keyboard callbacks. This example
demonstrate a couple ways to workaround this limitation.


## [Promises in clientside callbacks](./promise-clientside-callback)

Use a clientside callback that calls `Plotly.newPlot` from the
[`plotly.js` API](https://plotly.com/javascript/plotlyjs-function-reference/#plotlynewplot)
(which returns a
[JavaScript Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise))
allowing us to transfer the figure
data from the backend to the frontend using custom format without (large) array duplication.


## [Interactive Gantt Chart app](./gantt-charts)

Example showing how to increasing add complexity to a Dash.jl app.
This example was used during my presentation at JuliaCon 2023.


## [Alternative developer mode](./alt-dev-mode)

Example of a [Revise](https://github.com/timholy/Revise.jl)-based in-REPL hot-reloadable,
[Infiltrator](https://github.com/JuliaDebug/Infiltrator.jl)-compatible
developer mode for Dash.jl.


## [Memoize long running callbacks](./memoize-callbacks)

Example app with memoized callbacks using
[`Memoization.jl`](https://github.com/marius311/Memoization.jl)
and a least-recently-used cache from
[`LRUCache.jl`](https://github.com/JuliaCollections/LRUCache.jl).


## [Show backend errors in Dash app UI](./show-backend-errors)

Technique for catching backend errors coming within a callback and
then showing them to the users in the Dash app UI.
