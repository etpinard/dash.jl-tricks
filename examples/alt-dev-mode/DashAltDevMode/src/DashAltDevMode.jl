module DashAltDevMode

using Revise
using Dash, Dash.HTTP, Dash.Sockets
using Dates

export watch

# mostly taken from
# https://github.com/plotly/Dash.jl/blob/bba2bcb8765935d5aa86660992edf275b163a741/src/server.jl#L52-L57
function start_server(app::Dash.DashApp, host, port)
    handler = Dash.make_handler(app)
    server = Sockets.listen(Dash.get_inetaddr(host, port))
    task = @async HTTP.serve(handler, host, port; server)
    return server, task
end

function watch(make_app::Function,
               files::Vector{String}=String[],
               modules::Vector{Module}=Module[],
               host=Dash.dash_env("HOST", "127.0.0.1"; prefix=""),
               port=Dash.dash_env(Int64, "PORT", 8050; prefix=""))
    @assert(!isempty(files) || !isempty(modules),
            "must track at least 1 file or 1 module")

    function go()
        @info "Updating..." Dates.now()
        app = make_app()
        Dash.enable_dev_tools!(app; debug=true)
        return start_server(app, host, port)[1]
    end

    server = go()
    # somewhat inspired by:
    # https://github.com/plotly/Dash.jl/blob/dev/src/utils/hot_restart.jl
    # thanks @waralex !
    while true
        try
            task = @async Revise.entr(files, modules; postpone=true) do
                close(server)
                return server = go()
            end
            wait(task)
        catch err
            if err isa InterruptException
                @info "Exiting!"
                return close(server)
            end
            @warn "Halting server" err
            close(server)
        end
    end
end

end # module DashAltDevMode
