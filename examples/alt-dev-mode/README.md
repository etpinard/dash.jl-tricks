# Alternative developer mode

Example of a [Revise](https://github.com/timholy/Revise.jl)-based in-REPL hot-reloadable,
[Infiltrator](https://github.com/JuliaDebug/Infiltrator.jl)-compatible
developer mode for Dash.jl.

## Usage

```jl
(alt-dev-mode) pkg> instantiate

julia> include("main.jl")

julia> using DashAltDevMode

julia> @async watch(make_app, ["main.jl"])

# OR
julia> @async watch(["main.jl"]) do
          # setup logic ..
          app = make_app()
          # ... maybe more setup logic
          return app
       end

# OR to watch for change in dependencies
julia> using SomeOtherLocalPackage
julia> @async watch(make_app, ["main.jl"], [SomeOtherLocalPackage])

# debug callbacks with `Infiltrator.jl`
julia> using Infiltrator

# then e.g.
shell> git diff

diff --git a/examples/alt-dev-mode/main.jl b/examples/alt-dev-mode/main.jl
index 358c2aa..162bb28 100644
--- a/examples/alt-dev-mode/main.jl
+++ b/examples/alt-dev-mode/main.jl
@@ -11,6 +11,7 @@ function make_app()
     end

     callback!(app, Output("my-div", "children"), Input("my-id", "value")) do input_value
+        @infiltrator
         return reply(input_value)
     end

# which leads to
┌ Info: Updating...
└   Dates.now() = 2023-07-20T17:26:01.433
[ Info: Listening on: 127.0.0.1:8050, thread id: 1
Infiltrating (::var"#8#10"{Dash.DashApp})(input_value::String)
  at /home/etienne/Documents/repos/dash.jl-tricks/examples/alt-dev-mode/main.jl:14

infil>

# and then allow you to e.g.
infil> @locals
- app::Dash.DashApp = Dash.DashApp("/home/etienne/Documents/repos/dash.jl-tricks/examples/alt-dev-mode", …
- input_value::String = "initial value"
```
