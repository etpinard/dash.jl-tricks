using Dash
using SomeOtherLocalPackage

function make_app()
    app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

    app.layout = html_div() do
        html_h1("Alternative developer mode for Dash.jl !!!"),
        dcc_input(id = "my-id", value = "initial value", type = "text"),
        html_div(id = "my-div")
    end

    callback!(app, Output("my-div", "children"), Input("my-id", "value")) do input_value
        return reply(input_value)
    end

    return app
end

if abspath(PROGRAM_FILE) == @__FILE__
    app = make_app()
    run_server(app)
end
