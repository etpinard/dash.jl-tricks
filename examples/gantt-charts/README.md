# Interactive Gantt Chart app

Example showing how to increasing add complexity to a Dash.jl app.
This example was used during my presentation at JuliaCon 2023.

## App snapshots

This example offers several app _snapshots_. This should allow users get
increasing comfortable with the increasing complexity of a Dash.jl app as one
adds more features.

- [`01.jl`](apps/01.jl)
  + simple `dcc_dropdown` and `dcc_graph` app

- in [`02[a-b].jl`](apps/02b.jl)
  + more Julian callback pattern
  + use `multi=true` in `dcc_dropdown`
  + a first callback handling nothing/empty/JSON3 values
  + **N.B.** dropped from JuliaCon 2023 talk

- in [`03[a-b].jl`](apps/03b.jl)
  + a callback handling `dcc_graph`'s `clickData`

- in [`04[a-c].jl`](apps/04c.jl)
  * introduce `from_dash`
  * more julian `addcallback!`

## Usage

```sh
# install deps
julia --project -e 'import Pkg; Pkg.instantiate()'

# run the app (on port 8050)
julia --project apps/02.jl

# or choose the port with:
PORT=8123 julia --project apps/02.jl
```

## Run _equivalent python version_ of `apps/01.jl`

```sh
python -m venv venv
. venv/bin/activate
pip install dash
python apps/01.py
```
