using Dash

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px")),

    dcc_graph(; id="my-graph")
end

callback!(app,
          Output("my-graph", "figure"),
          Input("my-dropdown", "value")) do fname
    return isnothing(fname) ? NamedTuple() : make_gantt(fname)
end

run_server(app; debug=true)
