from dash import Dash, html, dcc, callback, Output, Input
import os

from _utils import PATH_DATA, make_gantt

app = Dash(external_stylesheets=["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html.Div([
    html.H1("Dash at JuliaCon 2023"),

    dcc.Dropdown(id="my-dropdown",
                 options=[{"label": os.path.splitext(fname)[0], "value": fname}
                          for fname in os.listdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style={"width": "400px"}),

    dcc.Graph(id="my-graph")
])

@callback(Output("my-graph", "figure"),
          Input("my-dropdown", "value"))
def update_graph(fname):
    return {} if fname is None else make_gantt(fname)

app.run(debug=True)
