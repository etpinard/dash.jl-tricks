using Dash
using JSON3
using DataFrames

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt, load_df

function Utils.make_gantt(fnames::JSON3.Array)
    df = mapreduce(load_df, vcat, fnames; init=DataFrame())
    return make_gantt(df)
end

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px"),
                 multi=true),

    dcc_graph(; id="my-graph")
end

callback!(app,
          Output("my-graph", "figure"),
          Input("my-dropdown", "value")) do fnames
    return if isnothing(fnames) || isempty(fnames)
        NamedTuple()
    else
        make_gantt(fnames)
    end
end

run_server(app; debug=true)
