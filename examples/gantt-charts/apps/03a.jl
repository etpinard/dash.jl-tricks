using Dash
using JSON3

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px")),

    dcc_graph(; id="my-graph"),

    html_details() do
        html_summary("Click Data"),
        html_pre(; id="log")
    end
end

callback!(app,
          Output("my-graph", "figure"),
          Input("my-dropdown", "value")) do fname
    return isnothing(fname) ? NamedTuple() : make_gantt(fname)
end

callback!(app,
          Output("log", "children"),
          Input("my-graph", "clickData"),
          State("my-dropdown", "value")) do clickdata, fname
    return string((; clickdata, fname))
end

run_server(app; debug=true)
