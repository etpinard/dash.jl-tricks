using Dash
using JSON3
using Dates

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px")),

    dcc_graph(; id="my-graph"),

    html_details() do
        html_summary("Click Data"),
        html_pre(; id="log")
    end,

    html_div(; id="closeup")
end

callback!(app,
          Output("my-graph", "figure"),
          Input("my-dropdown", "value")) do fname
    return isnothing(fname) ? NamedTuple() : make_gantt(fname)
end

function update_closeup(clickdata::JSON3.Object, fname::AbstractString)
    log = string((; clickdata, fname))

    customdata = try
        clickdata.points[1].customdata
    catch err
        []
    end

    if isempty(customdata)
        return update_closeup(nothing)
    end

    pt_start, pt_end = DateTime.(customdata)
    range = [pt_start-Day(1), pt_end+Day(1)]

    (; data, layout) = make_gantt(fname)
    layout2 = (; layout...,
              title="Close up!",
              xaxis=(; layout.xaxis..., range))
    figure = (; data, layout=layout2)

    return log, dcc_graph(; figure)
end
update_closeup(args...) = "empty!", nothing

callback!(update_closeup,
          app,
          Output("log", "children"),
          Output("closeup", "children"),
          Input("my-graph", "clickData"),
          State("my-dropdown", "value"))

run_server(app; debug=true)
