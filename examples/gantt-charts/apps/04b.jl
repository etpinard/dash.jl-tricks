using Dash
using JSON3
using Dates

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt

abstract type AbstractCallbackValue end

from_dash(::Type{<:AbstractCallbackValue}, ::Any) = nothing

struct FName <: AbstractCallbackValue
    value::String
end

from_dash(T::Type{<:FName}, value::AbstractString) = FName(String(value))

struct ClickedPoint <: AbstractCallbackValue
    pt_start::DateTime
    pt_end::DateTime
end

function from_dash(T::Type{<:ClickedPoint}, clickdata::JSON3.Object)
    # N.B. use try-catch for simplicity
    return try
        pt_start, pt_end = DateTime.(clickdata.points[1].customdata)
        ClickedPoint(pt_start, pt_end)
    catch err
        nothing
    end
end

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px")),

    dcc_graph(; id="my-graph"),

    html_details() do
        html_summary("Click Data"),
        html_pre(; id="log")
    end,

    html_div(; id="closeup")
end

update_figure(fname::FName) = make_gantt(fname.value)
update_figure(args...) = NamedTuple()

callback!(app,
          Output("my-graph", "figure"),
          Input("my-dropdown", "value")) do args...
    return update_figure(from_dash(FName, args[1]))
end

function update_closeup(pt::ClickedPoint, fname::FName)
    log = string((; pt, fname))

    range = [pt.pt_start-Day(1), pt.pt_end+Day(1)]

    (; data, layout) = make_gantt(fname.value)
    layout2 = (; layout...,
              title="Close up!",
              xaxis=(; layout.xaxis..., range))
    figure = (; data, layout=layout2)

    return log, dcc_graph(; figure)
end
update_closeup(args...) = "empty!", nothing

callback!(app,
          Output("log", "children"),
          Output("closeup", "children"),
          Input("my-graph", "clickData"),
          State("my-dropdown", "value")) do args...
    return update_closeup(from_dash(ClickedPoint, args[1]),
                          from_dash(FName, args[2]))
end

run_server(app; debug=true)
