using Dash
using JSON3
using Dates

include("_Utils.jl"); using .Utils: PATH_DATA, make_gantt

abstract type AbstractCallbackValue end

from_dash(::Type{<:AbstractCallbackValue}, ::Any) = nothing

struct FName <: AbstractCallbackValue
    value::String
end

from_dash(T::Type{<:FName}, value::AbstractString) = FName(String(value))

struct ClickedPoint <: AbstractCallbackValue
    pt_start::DateTime
    pt_end::DateTime
end

function from_dash(T::Type{<:ClickedPoint}, clickdata::JSON3.Object)
    # N.B. use try-catch for simplicity
    return try
        pt_start, pt_end = DateTime.(clickdata.points[1].customdata)
        ClickedPoint(pt_start, pt_end)
    catch err
        nothing
    end
end

abstract type AbstractDepJl{T} end

from_dash(::AbstractDepJl{T}, val) where {T} = from_dash(T, val)

struct Input_jl{T} <: AbstractDepJl{T}
    dep::Dash.Dependency
end
Input_jl(T, compid, prop) = Input_jl{T}(Input(compid, prop))

struct State_jl{T} <: AbstractDepJl{T}
    dep::Dash.Dependency
end
State_jl(T, compid, prop) = State_jl{T}(State(compid, prop))

_stubdep(depjl::AbstractDepJl{T}) where {T} = depjl.dep
_stubdep(dep::Dash.Dependency) = dep

_split_deps(T, deps) = collect(filter(x -> isa(x, T), deps))

function add_callback!(func::Function, app::Dash.DashApp, args...; kwargs...)
    outputs = _split_deps(Output, args)
    inputs = _split_deps(Union{Input, Input_jl}, args)
    states = _split_deps(Union{State, State_jl}, args)

    inputs2 = _stubdep.(inputs)
    states2 = _stubdep.(states)
    args2 = vcat(outputs, inputs2, states2)

    func2(vals...) = begin
        vals2 = map(enumerate(vals)) do (i, val)
            dep = vcat(inputs, states)[i]
            if isa(dep, AbstractDepJl)
                from_dash(dep, val)
            else
                val
            end
        end
        return func(vals2...)
    end

    return callback!(func2, app, args2...; kwargs...)
end

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Dash.jl at JuliaCon 2023"),

    dcc_dropdown(; id="my-dropdown",
                 options=[(label=first(splitext(fname)), value=fname)
                          for fname in readdir(PATH_DATA)],
                 placeholder="Please select a data file",
                 style=(; width="400px")),

    dcc_graph(; id="my-graph"),

    html_details() do
        html_summary("Click Data"),
        html_pre(; id="log")
    end,

    html_div(; id="closeup")
end

update_figure(fname::FName) = make_gantt(fname.value)
update_figure(::Nothing) = NamedTuple()

add_callback!(update_figure,
             app,
             Output("my-graph", "figure"),
             Input_jl(FName, "my-dropdown", "value"))

function update_closeup(pt::ClickedPoint, fname::FName)
    log = string((; pt, fname))
    range = [pt.pt_start-Day(1), pt.pt_end+Day(1)]

    (; data, layout) = make_gantt(fname.value)
    layout2 = (; layout...,
              title="Close up!",
              xaxis=(; layout.xaxis..., range))
    figure = (; data, layout=layout2)

    return log, dcc_graph(; figure)
end
update_closeup(args...) = "empty!", nothing

add_callback!(update_closeup,
              app,
              Output("log", "children"),
              Output("closeup", "children"),
              Input_jl(ClickedPoint, "my-graph", "clickData"),
              State_jl(FName, "my-dropdown", "value"))

run_server(app; debug=true)
