module Utils

using CSV, DataFrames, Dates

const PATH_DATA = joinpath(@__DIR__, "data")

load_df(fname) = DataFrame(CSV.File(joinpath(PATH_DATA, fname)))

function make_gantt(df::AbstractDataFrame)
    @assert names(df) == ["Name", "Start", "End"]
    @assert eltype(df.Start) == DateTime
    @assert eltype(df.End) == DateTime

    # `x` correspond to the span of the bar
    y = df.Name
    base = df.Start
    x = (df.End .- df.Start) .|> Dates.value

    # show "raw" `Start` and `End` values in hover labels
    customdata = collect(zip(df.Start, df.End))
    hovertemplate = "<b>Start:</b> %{customdata[0]}<br>" *
                    "<b>End:</b>  %{customdata[1]}" *
                    "<extra>%{y}</extra>"

    # N.B. `type="date"` so that `x` Int get interpreted as time durations
    xaxis = (type="date",)

    return (data = [(; type="bar", orientation="h",
                     y, base, x, customdata, hovertemplate)],
            layout = (; title="Gantt chart",
                      xaxis))
end

make_gantt(fname::AbstractString) = make_gantt(load_df(fname))

end  # module Utils
