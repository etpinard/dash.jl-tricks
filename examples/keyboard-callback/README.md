# Keyboard callbacks in Dash.jl

Dash.jl nor the python version of dash support keyboard callbacks. This example
demonstrate a couple ways to workaround this limitation.

## Usage

```sh
julia --project -e 'import Pkg; Pkg.instantiate()'
julia --project main.jl
```
