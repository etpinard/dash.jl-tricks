function handleEnter (evt) {
  if (evt && evt.keyCode === 13) {
    const btnId = 'btn-enter'
    const btn = document.getElementById(btnId)
    if (btn) {
      btn.click()
    } else {
      console.warn('Could not find on page ' + btnId)
    }
  }
}

function handleGeneralStep1 (evt) {
  if (evt) {
    const btnId = 'btn-general'
    const btn = document.getElementById(btnId)
    if (btn) {
      const info = JSON.stringify({
        keyCode: evt.keyCode,
        shiftKey: evt.shiftKey,
        ctrlKey: evt.ctrlKey,
        altKey: evt.altKey
      })
      window.__dashEventInfo = info
      btn.click()
    } else {
      console.warn('Could not find on page ' + btnId)
    }
  }
}

document.addEventListener('keyup', handleEnter)
document.addEventListener('keyup', handleGeneralStep1)
