using Dash

const style_hidden = (visibility = "hidden",)

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_h1("Keyboard callbacks in Dash.jl"),
    html_h2("press on any key ..."),
    # listen for the <enter> key press, then click on invisible button
    html_div() do
        html_button(; id = "btn-enter", style = style_hidden),
        html_div(; id = "out-enter")
    end,
    # listen for a general key press then propagate event info to store,
    # then use store data
    html_div() do
        html_button(; id = "btn-general", style = style_hidden),
        dcc_store(id = "store-general"),
        html_div(; id = "out-general")
    end
end

callback!(app,
          Output("out-enter", "children"),
          Input("btn-enter", "n_clicks")) do n_enter
    return if isnothing(n_enter)
        nothing
    elseif n_enter == 1
        "You pressed ENTER once."
    elseif n_enter > 1
        "You pressed ENTER $n_enter times."
    else
        "Not sure what's going on ..."
    end
end

callback!("""
          function handleGeneralStep2 (__trigger__) {
            return window.__dashEventInfo
          }
          """,
          app,
          Output("store-general", "data"),
          Input("btn-general", "n_clicks"))

callback!(app,
          Output("out-general", "children"),
          Input("store-general", "data")) do info
    return info
end

run_server(app, "0.0.0.0", 8080; debug = true)
