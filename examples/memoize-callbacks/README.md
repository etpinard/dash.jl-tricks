# Memoize long running callbacks

Example app with memoized callbacks using
[`Memoization.jl`](https://github.com/marius311/Memoization.jl)
and a least-recently-used cache from
[`LRUCache.jl`](https://github.com/JuliaCollections/LRUCache.jl).

## How to run this thing?

```sh
julia --project -e 'import Pkg; Pkg.instantiate()'
julia --project main.jl
```

## Useful links

- <https://github.com/marius311/Memoization.jl>
- <https://github.com/JuliaCollections/LRUCache.jl>
