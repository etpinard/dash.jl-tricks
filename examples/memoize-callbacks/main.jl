using Dash
using Memoization
using LRUCache

const maxsize = 10_000

function fibonacci(n::Int)
    return if n <= 2
        1
    else
        fibonacci(n - 1) + fibonacci(n - 2)
    end
end

@memoize LRU(; maxsize) function some_long_running_func(n)
    sleep(5)
    return fibonacci(n)
end

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

app.layout = html_div() do
    html_div(; className="container") do
        html_h1("Compute Fibonacci number"),
        dcc_input(; id = "input", type = "number", placeholder = "Enter n", debounce = true),
        html_br(),
        html_h3(; id = "output")
    end
end

handle(n::Int) = some_long_running_func(n)
handle(::Nothing) = nothing
handle(::Any) = "invalid input!"

callback!(handle, app, Output("output", "children"), Input("input", "value"))

run_server(app)
