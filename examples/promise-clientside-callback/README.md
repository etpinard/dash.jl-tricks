# Promises in clientside callbacks

Use a clientside callback that calls `Plotly.newPlot` from the
[`plotly.js` API](https://plotly.com/javascript/plotlyjs-function-reference/#plotlynewplot)
(which returns a
[JavaScript Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise))
allowing us to transfer the figure
data from the backend to the frontend using custom format without (large) array duplication.

## Requirements

- `Dash.jl >= 1.3`

## Usage

```sh
julia --project -e 'import Pkg; Pkg.instantiate()'
julia --project main.jl
```

## More _Promises in clientside callbacks_

The (python) Dash user guide gives an [example][example], which can be
translated to Julia as

<details>
<summary>see Julia snippet</summary>

```julia
using Dash

app = dash()

app.layout = html_div() do
    dcc_dropdown(; id = "data-select",
                 options = [(label="Car-sharing data",
                             value="https://raw.githubusercontent.com/plotly/datasets/master/carshare_data.json"),
                            (label="Iris data",
                             value="https://raw.githubusercontent.com/plotly/datasets/master/iris_data.json")]),
    html_br(),
    dash_datatable(; id = "my-table-promises",
                   page_size = 10)
end

callback!("""
          async function (value) {
            const response = await fetch(value)
            const data = await response.json()
            return data
          }
          """,
          app,
          Output("my-table-promises", "data"),
          Input("data-select", "value"))

run_server(app, "0.0.0.0", 8080)
```

</details>

[example]: https://dash.plotly.com/clientside-callbacks#clientside-callbacks-with-promises
