using Dash
using Dates: now, Minute
using Random: rand

const N = 500_000
const n = 10

function make_xys()
    # some irregular grid
    x = now() .+ [Minute(i + rand(0:1)) for i in 1:N]
    # generate `n` traces
    ys = map(_ -> sin.(1:N) .* rand(N), collect(1:n))
    return x, ys
end

make_layout() = (plot_bgcolor="#d3d3d3",)

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"],
           # No need to execute callbacks before first button click
           prevent_initial_callbacks = true)

app.layout = html_div() do
    html_div() do
        html_button("Plot slow!"; id = "btn-go-slow"),
        html_h3("Standard figure payload"),
        dcc_graph(; id = "graph-slow")
    end,
    html_div() do
        html_button("Plot fast!"; id = "btn-go-fast"),
        html_h3("Custom figure payload"),
        dcc_store(; id = "store-fast"),
        dcc_graph(; id = "graph-fast")
    end
end

callback!(app,
          Output("graph-slow", "figure"),
          Input("btn-go-slow", "n_clicks")) do go
    @time begin
        x, ys = make_xys()
        # standard plotly figure
        data = [(; mode="lines", x, y) for y in ys]
        # some common layout
        layout = make_layout()
    end
    return (; data, layout)
end

callback!(app,
          Output("store-fast", "data"),
          Input("btn-go-fast", "n_clicks")) do go
    @time begin
        x, ys = make_xys()
        # N.B. custom plotly figure, input `x` just once!
        xs = vcat([x], fill("[0, \"x\"]", n-1))
        data = [(; mode="lines", x, y) for (x, y) in zip(xs, ys)]
        # some common layout
        layout = make_layout()
    end
    return (; data, layout)
end

callback!("""
          function plot (fig) {
            const data = fig.data || []
            const layout = fig.layout || {}

            const data2 = data.map(trace => {
              let x
              if (typeof trace.x === "string") {
                const loc = JSON.parse(trace.x)
                x = data[loc[0]][loc[1]]
              } else {
                x = trace.x
              }
              return {mode: trace.mode, x: x, y: trace.y}
            })

            return Plotly.newPlot("graph-fast", {data: data2, layout: layout})
          }
          """,
          app,
          Output("graph-fast", "figure"),
          Input("store-fast", "data"))


@time "make_xys warmup" make_xys()

run_server(app, "0.0.0.0", 8080)

#=

Some numbers:

|                          | fast      | slow      |
|--------------------------|-----------|-----------|
| gzip'ed payload          | 46.7 MB   | 55.8 MB   |
| callback time            | 8761 ms   | 15_589 ms |
| clientside callback time | 5020 ms   | n/a       |
| total time-to-plot       | 13_781 ms | 15)589 ms |

+#
