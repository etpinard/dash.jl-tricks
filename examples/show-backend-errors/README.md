# Show backend errors in Dash app UI

Technique for catching backend errors coming within a callback and
then showing them to the users in the Dash app UI.

## Usage

```sh
julia --project -e 'import Pkg; Pkg.instantiate()'
julia --project main.jl
```
