using Dash

app = dash(; external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"])

module Sandbox end

function backtrace_md(err::Exception)
    io = IOBuffer()
    showerror(io, err, catch_backtrace())
    out = String(take!(io))
    return """```
$out
```"""
end

app.layout = html_div() do
    html_div(; className = "container") do
        html_h1("Evaluate Julia expressions"),
        dcc_textarea(; id = "input", placeholder = "Enter expression",
                     style = (width = "600px", minHeight = "200px")),
        html_br(),
        html_button("Run"; id = "button"),
        html_br(),
        html_div(html_pre(; id = "output")),
        html_div(dcc_markdown(; id = "error"))
    end
end

function handle(::Int, txt)
    @info "valid" txt
    return try
        out = Sandbox.eval(Meta.parse(txt))
        (out, nothing)
    catch err
        (nothing, backtrace_md(err))
    end
end
function handle(args...)
    @info "fallback" args
    return nothing, nothing
end

callback!(handle,
          app,
          Output("output", "children"),
          Output("error", "children"),
          Input("button", "n_clicks"),
          State("input", "value"))

run_server(app)
