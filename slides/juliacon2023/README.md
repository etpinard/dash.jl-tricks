# Tips for writing and maintaining Dash.jl applications

My JuliaCon 2023 [presentation](https://pretalx.com/juliacon2023/talk/YGJ9SL/).

- [standalone `mkv` file of the presentation](./assets/pres.mkv)
- [link to the video platform that starts with _Y_ own by the company that starts with _G_](https://www.youtube.com/watch?v=SakBEs3O6Aw)

## How to build this thing?

I used [Quarto](https://quarto.org/), more specifically the
[revealjs](https://quarto.org/docs/presentations/revealjs/) renderer.

Installation information is available here: <https://quarto.org/docs/get-started/>

Then,

```sh
./bin/build.sh

# or with server + file-watcher for `index.qmd`
./bin/watch.sh
```

## Start live demo apps

This presentations includes a few slides with `<iframe>` pointing to local servers, allowing
for _LIVE_ demos within the presentation. Boot these servers with:

```sh
./bin/runall.sh
```

## Build all-static site

To build a version with the `<iframe>` replaced by screencast `webm` videos use:

```sh
./bin/build.sh static
```

Publicly available: <https://etpinard.gitlab.io/dash.jl-tricks/juliacon2023>
