#! /bin/bash

HERE=$(dirname "$(readlink -f "$0")")
FLAVOUR=${1-interactive}

pushd "$HERE/../assets" > /dev/null || return
make all
popd > /dev/null || return

pushd "$HERE/../demos" > /dev/null || return
for p in "$FLAVOUR"/*.qmd; do
    echo "linking $p"
    ln -fs "$p" "$(basename "$p")"
done
popd > /dev/null || return

pushd "$HERE/.." > /dev/null || return
echo "quarto render"
quarto render index.qmd --to revealjs
popd > /dev/null || return
