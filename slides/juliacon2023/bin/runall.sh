#! /bin/bash

HERE=$(dirname "$(readlink -f "$0")")

echo 'Booting Dash.jl servers in background!'

pushd $HERE/../../../examples/gantt-charts > /dev/null

julia --project -e 'import Pkg; Pkg.instantiate()'

PORT=9001 julia --project apps/01.jl &

PORT=9031 julia --project apps/03a.jl &
PORT=9032 julia --project apps/03b.jl &

popd > /dev/null
