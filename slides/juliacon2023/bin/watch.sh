#! /bin/bash

HERE=$(dirname "$(readlink -f "$0")")

cd "$HERE" || return
. build.sh

pushd "$HERE/.." > /dev/null || return
quarto preview index.qmd --to revealjs --no-browser
popd > /dev/null || return
