## More Julian ins-and-outs of callbacks

callbacks often have validation steps

```{.julia code-line-numbers="31,37-41,43-45"}
{{< include assets/apps/03b.jl >}}
```
::: {.notes}
- Ok now, I'd like to discuss ways to make Dash.jl
  callbacks more _Julian_
- looking back at our dropdown select-to-graph callback
:::

## More Julian ins-and-outs of callbacks

consider _callback value_ types and a `from_dash` function

```{.julia code-line-numbers="7,11-13,17-20|9,15,22-30"}
{{< include assets/apps/04b.jl >}}
```
::: {.notes}
- _callback value_ or _property value_
- where `from_dash` would have all the set-or-not-set logic
- call-all dispatch on the abstract type
- easily reusable if multiple callbacks have this property
  as `Input` or `State`.
:::

## More Julian ins-and-outs of callbacks

callback declaration, validation and update logic are now split nicely

```{.julia code-line-numbers="62-75,77-84|77-84"}
{{< include assets/apps/04b.jl >}}
```
::: {.notes}
- closeup on what's left of our `callback!`,
- would be easy to generalize ...
:::

## More Julian ins-and-outs of callbacks

easy to write a more _Julian_ `add_callback` function where we
declare the Julia types associated with the callback values

```{.julia code-line-numbers="118-123|122-123"}
{{< include assets/apps/04c.jl >}}
```
::: {.notes}
- with new type `Input_jl` and `State_jl`
  or just new `Input` and `State` method (would not type-piracy)

- leave the `add_callback!` definition
:::
